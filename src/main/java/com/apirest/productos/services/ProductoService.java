package com.apirest.productos.services;

import com.apirest.productos.models.Producto;
import com.apirest.productos.repositories.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    public List<Producto> findAll(){
        return productoRepository.findAll();
    }

    public Optional<Producto> findById(String id){
        return this.productoRepository.findById(id);
    }

    public Producto save(Producto producto) {
        return productoRepository.save(producto);
    }

    public boolean delete(Producto producto){
        try{
            productoRepository.delete(producto);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
