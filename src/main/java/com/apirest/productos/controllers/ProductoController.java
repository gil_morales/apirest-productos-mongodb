package com.apirest.productos.controllers;

import com.apirest.productos.models.Producto;
import com.apirest.productos.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<Producto> getProductos(){
        return this.productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity<Optional<Producto>> getProductoId(@PathVariable String id){
        Optional<Producto> producto = this.productoService.findById(id);
        if(producto == null || producto.isEmpty()){
            return new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(producto);
    }

    @PostMapping("/productos")
    public ResponseEntity postProductos(@RequestBody Producto newProducto){
        productoService.save(newProducto);
        return new ResponseEntity("Producto creado correctamente", HttpStatus.OK);
    }

    @PutMapping("/productos")
    public ResponseEntity putProductos(@RequestBody Producto productoToUpdate){
        Optional<Producto> producto = productoService.findById(productoToUpdate.getId());
        if(producto == null || producto.isEmpty()){
            return new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        productoService.save(productoToUpdate);
        return new ResponseEntity("Producto actualizado correctamente", HttpStatus.OK);
    }

    @DeleteMapping("/productos")
    public ResponseEntity deelteProductos(@RequestBody Producto productoToDelete){
        Optional<Producto> producto = productoService.findById(productoToDelete.getId());
        if(producto == null || producto.isEmpty()){
            return new ResponseEntity("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        productoService.delete(productoToDelete);
        return new ResponseEntity("Producto eliminado correctamente", HttpStatus.OK);
    }
}
